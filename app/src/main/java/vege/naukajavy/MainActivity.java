package vege.naukajavy;

import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import vege.naukajavy.database.DatabaseHelper;




public class MainActivity extends AppCompatActivity {

    DatabaseHelper myDbHelper;
    List<String> lista=new ArrayList<>();

    @BindView(R.id.editText) EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


//       editText.setText(sqlConnection.getQuestion(1));
        myDbHelper=new DatabaseHelper(this);
        try {
            myDbHelper.createDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        myDbHelper.openDatabase();

        lista.addAll(myDbHelper.getAllQuestions());
        editText.setText(lista.get(0));
    }
}
