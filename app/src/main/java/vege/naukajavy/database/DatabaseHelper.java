package vege.naukajavy.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Vege on 04.06.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper{

   String DB_PATH=null;
    private static String DB_NAME= "testy_java.db";
    private SQLiteDatabase db;

    private static final int DB_VERSION=10;
    public final Context myContext;
    private static final String id="Id";
    private static String question="Question";
    private static final String answer="Answer";
    private static String table="Questions";



    public DatabaseHelper(Context context){
        super(context, DB_NAME, null,DB_VERSION);
        myContext=context;
        DB_PATH=myContext.getDatabasePath(DB_NAME).toString();
        Log.e("Path 1",DB_PATH);
    }
    public void createDatabase() throws IOException{
        boolean dbExist=checkDatabase();
        if(dbExist){

        }
        else {
            this.getReadableDatabase();
            try {
                copyDatabase();
            }catch (IOException e){
                throw new Error("Error copying database");
            }
        }
    }
    private boolean checkDatabase(){
        SQLiteDatabase checkDB=null;
        try{
            String myPath=DB_PATH+DB_NAME;
            checkDB=SQLiteDatabase.openDatabase(myPath,null,SQLiteDatabase.OPEN_READONLY);
        }catch (SQLiteException e){

        }
        if(checkDB!=null){
            checkDB.close();
        }
        return checkDB!=null?true:false;
    }
//    private void copyDatabase() throws IOException {
//        InputStream myInput=myContext.getAssets().open(DB_NAME);
//        String outFileName=DB_PATH+DB_NAME;
//        OutputStream myOutput=new FileOutputStream(outFileName);
//        byte[] buffer=new byte[10];
//        int lenght;
//        while ((lenght=myInput.read(buffer))>0){
//            myOutput.write(buffer,0,lenght);
//        }
//        myOutput.flush();
//        myOutput.close();
//        myInput.close();
//    }

    public void openDatabase() throws SQLiteException {
        String myPath=DB_PATH+DB_NAME;
        db=SQLiteDatabase.openDatabase(myPath,null,SQLiteDatabase.OPEN_READONLY);
    }
    @Override
    public synchronized void close() {
        if (db != null)
            db.close();
        super.close();
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        String create_table="CREATE TABLE Questions(ID INTEGER PRIMARY KEY, QUESTION TEXT);";
        db.execSQL(create_table);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion)
            try {
                copyDatabase();
            } catch (IOException e) {
                e.printStackTrace();

            }
    }

    public Cursor query(String table, String[] columns, String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
        return db.query("EMP_TABLE", null, null, null, null, null, null);
    }
    public List<String> getAllQuestions(){
        List<String> list=new ArrayList<>();
        db=this.getReadableDatabase();
        //table="Questions";
        String selectAll="SELECT Question FROM Questions;";
        Cursor cursor=db.rawQuery(selectAll,null);
        if(cursor.moveToFirst())
        {
            do{
               // int id = cursor.getInt(0);
                String questionN=cursor.getString(1);
                Log.e("Question ",questionN);
                list.add(question);
            }
            while (cursor.moveToNext());
        }
        db.close();
        return list;
    }
}
